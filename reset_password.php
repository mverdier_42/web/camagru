<?php
include_once 'models/base.php';
include_once ROOT_PATH . '/models/checkResetPasswordToken.php';
include_once ROOT_PATH . '/models/resetPassword.php';
include_once ROOT_PATH . '/models/notify.php';

if (isset($_GET) && isset($_GET['token']))
	$token = checkResetPasswordToken($_GET['token']);
else if (isset($_POST) && isset($_POST['submit']) && $_POST['submit'] === 'Submit')
	resetPassword($_POST);
else
	notify('Your link is not valid', '/index.php');

include_once ROOT_PATH . '/views/reset_password.php';