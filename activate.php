<?php
include_once 'models/base.php';
include_once ROOT_PATH . '/models/activate.php';
include_once ROOT_PATH . '/models/notify.php';

if (!isset($_GET) || !isset($_GET['token']))
	notify('Your link is not valid', '/index.php');
else
	$activated = activate($_GET['token']);