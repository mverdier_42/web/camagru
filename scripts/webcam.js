var width = 600;
var height = 0;

var streaming = false;

var camera = null;
var video = null;
var capture = null;
var canvas1 = null;
var canvas2 = null;
var photo = null;
var photoFilter = null;
var save = null;
var uploadedImage = null;
var upload = null;
var image = null;
var filters = null;
var filter = null;
var filtersDiv = null;
var montageRightDiv = null;
var montageRightDivContent = null;
var scrollheight = null;

var limit = null;
var offset = null;

var reader = null;
var httpRequest = null;
var fullLoaded = false;

var dragActive = false;
var currentX;
var currentY;
var initialX;
var initialY;
var xOffset = 0;
var yOffset = 0;

function startup() {
	camera = document.getElementById('camera');
	video = document.getElementById('video');
	capture = document.getElementById('capture');
	canvas1 = document.getElementById('canvas1');
	canvas2 = document.getElementById('canvas2');
	photo = document.getElementById('photo');
	photoFilter = document.getElementById('photoFilter');
	save = document.getElementById('save');
	uploadedImage = document.getElementById('uploadedImage');
	upload = document.getElementById('upload');
	filters = document.getElementsByClassName('filters');
	filtersDiv = document.getElementById('filters');
	montageRightDiv = document.getElementById('montageRightDiv');
	montageRightDivContent = document.getElementById('montageRightDivContent');
	limit = 20;
	offset = 0;

	var constraints = { audio: true, video: true }; 
	navigator.mediaDevices.getUserMedia(constraints)
		.then(function(stream) {
			video.srcObject = stream;
			video.play();
		})
		.catch (function(error) {
		});

	video.addEventListener('canplay', function() {
		if (!streaming) {
			height = video.videoHeight / (video.videoWidth / width);
			video.setAttribute('width', width);
			video.setAttribute('height', height);
			canvas1.setAttribute('width', width);
			canvas1.setAttribute('height', height);
			canvas2.setAttribute('width', width);
			canvas2.setAttribute('height', height);
			camera.setAttribute('style', 'width:' + width + 'px !important;');
			camera.setAttribute('style', camera.getAttribute('style') + 'height:' + height + 'px !important');
			filtersDiv.setAttribute('width', width);
			streaming = true;
			if (filter)
				capture.disabled = false;
		}
	}, false);

	capture.addEventListener('click', function(event) {
		takePicture();
		event.preventDefault();
	}, false);

	save.addEventListener('click', function() {
		httpRequest = new XMLHttpRequest();
		httpRequest.onreadystatechange = saveRequest;
		httpRequest.open('POST', 'models/saveImage.php');
		httpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		httpRequest.send('image=' + photo.getAttribute('src') + '&filter=' + photoFilter.getAttribute('src')
			+ '&width=' + width + '&height=' + height);
	}, false);

	upload.addEventListener('click', function() {
		if (!height)
			height = 450;
		reader = new FileReader();
		reader.addEventListener('load', function() {
			if (image === null) {
				image = document.createElement('img');
				video.parentNode.replaceChild(image, video);
			}
			camera.setAttribute('style', 'width:' + width + 'px !important;');
			camera.setAttribute('style', camera.getAttribute('style') + 'height:' + height + 'px !important');
			image.setAttribute('width', width);
			image.setAttribute('height', height);
			image.setAttribute('z-index', 1);
			image.setAttribute('display', 'block');
			image.setAttribute('src', reader.result);
			if (filter)
				capture.disabled = false;
		}, false);
		if (uploadedImage.value)
			reader.readAsDataURL(uploadedImage.files[0]);
	}, false);

	for (var i = 0; i < filters.length; i++) {
		filters[i].addEventListener('click', filterClick);
	}

	montageRightDiv.addEventListener('scroll', montageRightScrollEvent, false);

	clearPhoto();
	loadMontageRight(montageRightRequest);
}

function montageRightScrollEvent(event) {
	event.preventDefault();
	var contentHeight = montageRightDivContent.offsetHeight;
	var y = montageRightDiv.scrollTop + montageRightDiv.offsetHeight;
	if (fullLoaded === false && y + 100 >= contentHeight) {
		offset += limit;
		loadMontageRight(montageRightRequest)
	}
}

function montageRightScroll() {
	var contentHeight = montageRightDivContent.offsetHeight;
	var y = montageRightDiv.scrollTop + montageRightDiv.offsetHeight;
	if (fullLoaded === false && y + 100 >= contentHeight) {
		offset += limit;
		loadMontageRight(montageRightRequest)
	}
}

function loadMontageRight(func, newLimit = false) {
	montageRightDiv.removeEventListener('scroll', montageRightScrollEvent);
	httpRequest = new XMLHttpRequest();
	httpRequest.onreadystatechange = func;
	httpRequest.open('POST', 'models/montageRight.php');
	httpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	if (newLimit === false)
		httpRequest.send('limit=' + limit + '&offset=' + offset);
	else {
		httpRequest.send('limit=' + newLimit + '&offset=' + offset);
		offset = newLimit;
	}
}

function montageRightRequest() {
	if (httpRequest.readyState === XMLHttpRequest.DONE) {
		if (httpRequest.status === 200) {
			if (httpRequest.responseText === 'end')
				fullLoaded = true;
			else {
				fullLoaded = false;
				montageRightDivContent.innerHTML += httpRequest.responseText;
			}
			montageRightDiv.addEventListener('scroll', montageRightScrollEvent, false);
			montageRightScroll();
		}
	}
}

function montageRightNewRequest() {
	if (httpRequest.readyState === XMLHttpRequest.DONE) {
		if (httpRequest.status === 200) {
			if (httpRequest.responseText === 'end')
				fullLoaded = true;
			else {
				fullLoaded = false;
				montageRightDivContent.innerHTML = httpRequest.responseText;
			}

			montageRightDiv.addEventListener('scroll', montageRightScrollEvent, false);
			montageRightScroll();
			montageRightDiv.scrollTop = 0;
		}
	}
}

function montageRightDelRequest() {
	if (httpRequest.readyState === XMLHttpRequest.DONE) {
		if (httpRequest.status === 200) {
			if (httpRequest.responseText === 'end') {
				fullLoaded = true;
				montageRightDivContent.innerHTML = '';
			}
			else {
				fullLoaded = false;
				montageRightDivContent.innerHTML = httpRequest.responseText;
			}
			montageRightDiv.scrollTop = scrollheight;
		}
	}
}

function clearPhoto() {
	var context1 = canvas1.getContext('2d');
	var context2 = canvas2.getContext('2d');
	context1.fillStyle = '#555555';
	context2.fillStyle = '#555555';
	context1.fillRect(0, 0, canvas1.width, canvas1.height);
	context2.fillRect(0, 0, canvas2.width, canvas2.height);

	var data1 = canvas1.toDataURL('image/jpeg');
	var data2 = canvas2.toDataURL('image/jpeg');
	photo.setAttribute('src', data1);
	photoFilter.setAttribute('src', data2);
}

function takePicture() {
	if (width && height) {
		canvas1.width = width;
		canvas1.height = height;
		canvas2.width = width;
		canvas2.height = height;
		var context1 = canvas1.getContext('2d');
		var context2 = canvas2.getContext('2d');

		if (image === null)
			context1.drawImage(video, 0, 0, width, height);
		else
			context1.drawImage(image, 0, 0, width, height);

		if (filter !== null)
			context2.drawImage(filter, filter.offsetLeft, filter.offsetTop, filter.width, filter.height);

		var data1 = canvas1.toDataURL('image/jpeg');
		var data2 = canvas2.toDataURL('image/png');
		photo.setAttribute('src', data1);
		photoFilter.setAttribute('src', data2);
		save.disabled = false;
	}
	else {
		clearPhoto();
	}
}

function saveRequest() {
	if (httpRequest.readyState === XMLHttpRequest.DONE) {
		if (httpRequest.status === 200) {
			offset = 0;
			loadMontageRight(montageRightNewRequest);
		}
	}
}

function filterClick() {
	if (!streaming && !image)
		return ;
	if (!filter) {
		filter = document.createElement('img');
		filter.setAttribute('class', 'filter')
		filter.setAttribute('src', this.getAttribute('src'));
		filter.setAttribute('width', width / 2);
		filter.setAttribute('height', 'auto');
		filter.addEventListener('mousedown', dragStart, false);
		filter.addEventListener('mousemove', drag, false);
		filter.addEventListener('mouseup', dragEnd, false);
		filter.addEventListener('touchstart', swipeStart, false);
		filter.addEventListener('touchmove', swipe, false);
		filter.addEventListener('touchend', swipeEnd, false);
		camera.insertBefore(filter, (image ? image : video));
		if (streaming || image)
			capture.disabled = false;
	}
	else if (this.getAttribute('src') === filter.getAttribute('src')) {
		camera.removeChild(filter);
		filter = null;
		capture.disabled = true;
		this.style.left = 0;
		this.style.top = 0;
	}
	else {
		camera.removeChild(filter);
		filter.setAttribute('src', this.getAttribute('src'));
		camera.insertBefore(filter, (image ? image : video));
		if (streaming || image)
			capture.disabled = false;
		this.style.left = 0;
		this.style.top = 0;
	}
}

function dragStart(event) {
	event.preventDefault();
	initialX = this.offsetLeft;
	initialY = this.offsetTop;
	xOffset = event.pageX;
	yOffset = event.pageY;
	dragActive = true;
}

function drag(event) {
	if (dragActive) {
		event.preventDefault();
		this.style.left = initialX + event.pageX - xOffset + 'px';
		this.style.top = initialY + event.pageY - yOffset + 'px';
	}
}

function dragEnd() {
	dragActive = false;
}

function swipeStart(event) {
	event.preventDefault();
	initialX = this.offsetLeft;
	initialY = this.offsetTop;
	var touch = event.touches;
	xOffset = touch[0].pageX;
	yOffset = touch[0].pageY;
	dragActive = true;
}

function swipe(event) {
	if (dragActive) {
		event.preventDefault();
		var contact = event.touches;
		this.style.left = initialX + contact[0].pageX - xOffset + 'px';
		this.style.top = initialY + contact[0].pageY - yOffset + 'px';
	}
}

function swipeEnd() {
	dragActive = false;
}

function filterZoom(event) {
	if (filter != null && event.key === '+') {
		dragActive = false;
		filter.width += 10;
	}
	else if (filter != null && event.key === '-') {
		dragActive = false;
		filter.width -= 10;
	}
}

function deleteMiniature(minID) {
	httpRequest = new XMLHttpRequest();
	httpRequest.onreadystatechange = deleteMiniatureRequest;
	httpRequest.open('POST', 'models/deleteImage.php');
	httpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	httpRequest.send('id=' + minID);
}

function deleteMiniatureRequest() {
	if (httpRequest.readyState === XMLHttpRequest.DONE) {
		if (httpRequest.status === 200) {
			var newLimit = limit + offset;
			offset = 0;
			scrollheight = montageRightDiv.scrollTop;
			loadMontageRight(montageRightDelRequest, newLimit);
		}
	}
}

window.addEventListener('load', startup, false);
window.addEventListener('keypress', filterZoom, false);