(function() {
	var notif = null;

	function startup() {
		notif = document.getElementById('notif');
		fillNotif();
	}

	function fillNotif() {
		httpRequest = new XMLHttpRequest();
		httpRequest.onreadystatechange = fillNotifRequest;
		httpRequest.open('POST', 'models/getNotifPref.php');
		httpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		httpRequest.send();
	}

	function fillNotifRequest() {
		if (httpRequest.readyState === XMLHttpRequest.DONE) {
			if (httpRequest.status === 200) {
				if (httpRequest.responseText === 'true')
					notif.checked = true;
				else if (httpRequest.responseText === 'false')
					notif.checked = false;
			}
		}
	}

	window.addEventListener('load', startup, false);
})();