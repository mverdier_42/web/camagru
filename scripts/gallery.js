var gallery = null;
var limit = null;
var offset = null;
var fullLoaded = false;
var scrollHeight = null;

function startup(imageID = null, src = null, own = null, likesNb = null, commentsNb = null, timestamp = null, ownerLogin = null, func = null) {
	gallery = document.getElementById('gallery');
	limit = 20;
	offset = 0;
	getImages(getImagesRequest);

	if (func != null) {
		// Let the getImages() get all images before display the requested image.
		setTimeout(function() { func(imageID, src, own, likesNb, commentsNb, timestamp, ownerLogin);}, 200);
	}
}

function getImages(func, newLimit = false) {
	window.removeEventListener('scroll', loadImagesEvent);
	httpRequest = new XMLHttpRequest();
	httpRequest.onreadystatechange = func;
	httpRequest.open('POST', 'models/getImages.php');
	httpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	if (newLimit === false)
		httpRequest.send('limit=' + limit + '&offset=' + offset);
	else {
		httpRequest.send('limit=' + newLimit + '&offset=' + offset);
		offset = newLimit - limit;
	}
}

function getImagesRequest() {
	if (httpRequest.readyState === XMLHttpRequest.DONE) {
		if (httpRequest.status === 200) {
			if (httpRequest.responseText === 'end')
				fullLoaded = true;
			else {
				fullLoaded = false;
				gallery.innerHTML += httpRequest.responseText;
				loadImages();
			}
			window.addEventListener('scroll', loadImagesEvent, false);	
		}
	}
}

function getImagesRefreshRequest() {
	if (httpRequest.readyState === XMLHttpRequest.DONE) {
		if (httpRequest.status === 200) {
			if (httpRequest.responseText === 'end')
				fullLoaded = true;
			else {
				fullLoaded = false;
				gallery.innerHTML = httpRequest.responseText;
			}
			window.addEventListener('scroll', loadImagesEvent, false);	
		}
	}
}

function loadImagesEvent(event) {
	event.preventDefault();
	var galleryHeight = gallery.offsetHeight;
	var yOffset = window.pageYOffset;
	var y = yOffset + window.innerHeight;
	if (fullLoaded === false && y + 100 >= galleryHeight) {
		offset += limit;
		y = 0;
		getImages(getImagesRequest);
	}
}

function loadImages() {
	var galleryHeight = gallery.offsetHeight;
	var yOffset = window.pageYOffset;
	var y = yOffset + window.innerHeight;
	if (fullLoaded === false && y + 100 >= galleryHeight) {
		offset += limit;
		y = 0;
		getImages(getImagesRequest);
	}
}

window.addEventListener('scroll', loadImagesEvent, false);	

var display = null;
var comments = null;
var inText = null;
var sendCom = null;
var id = null;
var owner = null;
var lNb = null;
var cNb = null;
var nbLikes = null;
var nbComments = null;
var imageInfos = null;

function displayImg(imageID, src, own, likesNb, commentsNb, timestamp, ownerLogin) {
	id = imageID;
	owner = own;
	lNb = likesNb;
	cNb = commentsNb;

	display = document.createElement('div');
	display.setAttribute('class', 'display');
	document.body.insertBefore(display, document.getElementById('galleryContainer'));

	var background = document.createElement('div');
	background.setAttribute('class', 'displayBackground');
	background.addEventListener('click', closeDisplay, false);
	display.appendChild(background);

	var bigFrame = document.createElement('div');
	bigFrame.setAttribute('class', 'displayBigFrame');
	display.insertBefore(bigFrame, background);

	var frame = document.createElement('div');
	frame.setAttribute('class', 'displayFrame');
	bigFrame.appendChild(frame);

	var image = document.createElement('img');
	image.setAttribute('src', '/images/' + src);
	image.setAttribute('class', 'displayImg');
	frame.appendChild(image);

	if (owner == true) {
		var del = document.createElement('div');
		del.setAttribute('class', 'deleteGallery');
		del.innerHTML = 'X';
		del.addEventListener('click', deleteImage, false);
		frame.appendChild(del);
	}

	var newL = document.createElement('br');
	frame.appendChild(newL);

	imageInfos = document.createElement('div');
	imageInfos.setAttribute('class', 'imageInfosRight');
	imageInfos.innerHTML = '[' + timestamp + '] ' + ownerLogin;
	frame.appendChild(imageInfos);

	nbLikes = document.createElement('div');
	nbLikes.setAttribute('class', 'displayText');
	nbLikes.setAttribute('cursor', 'pointer');
	nbLikes.addEventListener('click', like, false);
	nbLikes.innerHTML = '&#128077 ' + likesNb;
	frame.appendChild(nbLikes);

	nbComments = document.createElement('div');
	nbComments.setAttribute('class', 'displayText');
	nbComments.innerHTML = '&#128172 ' + commentsNb;
	frame.appendChild(nbComments);

	var newL = document.createElement('br');
	frame.appendChild(newL);

	// Comments display area.
	comments = document.createElement('div');
	comments.setAttribute('class', 'comments');
	frame.appendChild(comments);

	// Comment input text box.
	inText = document.createElement('input');
	inText.setAttribute('type', 'text');
	inText.setAttribute('class', 'commentInput');
	inText.addEventListener('keydown', sendComment, false);
	frame.appendChild(inText);

	// Send comment.
	sendCom = document.createElement('button');
	sendCom.addEventListener('click', sendComment, false);
	sendCom.innerHTML = 'Send';
	frame.appendChild(sendCom);
	
	getComments(id);
}

function closeDisplay() {
	var newLimit = limit + offset;
	offset = 0;
	scrollHeight = window.pageYOffset;
	getImages(getImagesRefreshRequest, newLimit);
	document.documentElement.scrollTop = scrollHeight;
	document.body.removeChild(display);
}

var httpRequest = null;

function getComments(id) {
	httpRequest = new XMLHttpRequest();
	httpRequest.onreadystatechange = getCommentsRequest;
	httpRequest.open('POST', 'models/getComments.php');
	httpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	httpRequest.send('id=' + id);
}

function getCommentsRequest() {
	if (httpRequest.readyState === XMLHttpRequest.DONE) {
		if (httpRequest.status === 200) {
			comments.innerHTML = httpRequest.responseText;
			comments.scrollTop = comments.scrollHeight;
		}
	}
}

function sendComment(event) {
	if (event.type === 'keydown' && event.key !== 'Enter')
		return ;
	if (inText.value.length > 255) {
		alert('WARNING: This comment is too long [255 chars max]');
		return ;
	}
	httpRequest = new XMLHttpRequest();
	httpRequest.onreadystatechange = sendCommentRequest;
	httpRequest.open('POST', 'models/sendComment.php');
	httpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	httpRequest.send('id=' + id + '&text=' + inText.value);
}

function sendCommentRequest() {	
	if (httpRequest.readyState === XMLHttpRequest.DONE) {
		if (httpRequest.status === 200) {
			if (httpRequest.responseText === 'false')
				window.location.reload();
			else {
				cNb++;
				nbComments.innerHTML = '&#128172 ' + cNb;
				comments.innerHTML += httpRequest.responseText;
				comments.scrollTop = comments.scrollHeight;
				inText.value = '';
			}
		}
	}
}

function like() {
	httpRequest = new XMLHttpRequest();
	httpRequest.onreadystatechange = likeRequest;
	httpRequest.open('POST', 'models/sendLike.php');
	httpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	httpRequest.send('id=' + id);
}

function likeRequest() {
	if (httpRequest.readyState === XMLHttpRequest.DONE) {
		if (httpRequest.status === 200) {
			if (httpRequest.responseText === 'like') {
				lNb++;
				nbLikes.innerHTML = '&#128077 ' + lNb;
			}
			else if (httpRequest.responseText === 'unlike') {
				lNb--;
				nbLikes.innerHTML = '&#128077 ' + lNb;
			}
			else if (httpRequest.responseText === 'false')
				window.location.reload();
		}
	}
}

function deleteImage() {
	httpRequest = new XMLHttpRequest();
	httpRequest.onreadystatechange = deleteImageRequest;
	httpRequest.open('POST', 'models/deleteImage.php');
	httpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	httpRequest.send('id=' + id);
}

function deleteImageRequest() {
	if (httpRequest.readyState === XMLHttpRequest.DONE) {
		if (httpRequest.status === 200) {
			closeDisplay();
		}
	}
}