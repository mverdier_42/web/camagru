var notifs = null;

function startup() {
	notifs = document.getElementsByClassName('notifications');

	for (var i = 0; i < notifs.length; i++) {
		notifs[i].addEventListener('click', clearNotif, false);
	}
}

function clearNotif() {
	document.body.removeChild(this);
}

window.addEventListener('load', startup, false);