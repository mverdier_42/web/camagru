<?php
include_once 'models/base.php';
include_once ROOT_PATH . '/models/signin.php';
include_once ROOT_PATH . '/models/signup.php';
include_once ROOT_PATH . '/models/sendResetPassword.php';

if (isset($_SESSION['login'])) {
	header('Location: /profile.php');
	exit ;
}

if (isset($_POST) && isset($_POST['submit']) && $_POST['submit'] === 'Sign in')
	SignIn($_POST);
else if (isset($_POST) && isset($_POST['submit']) && $_POST['submit'] === 'Sign up')
	SignUp($_POST);
else if (isset($_POST) && isset($_POST['submit']) && $_POST['submit'] === 'Reset password')
	sendResetPassword($_POST['email']);

include_once ROOT_PATH . '/views/connexion.php';