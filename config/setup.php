<?php
include_once 'rootPath.php';
include_once ROOT_PATH . '/models/dbConnect.php';
include_once ROOT_PATH . '/models/tryQuery.php';

$hostDsn = 'mysql:host=localhost';

$host = dbConnect($hostDsn);

$dropDb = $host->prepare('DROP DATABASE IF EXISTS `camagru`;');
tryQuery($dropDb);

$createDb = $host->prepare('CREATE DATABASE `camagru`;');
tryQuery($createDb);

$db = dbConnect();

$createUser = $db->prepare("CREATE TABLE `user` (
				`id` int(11) NOT NULL AUTO_INCREMENT,
				`login` varchar(20) NOT NULL,
				`password` varchar(128) NOT NULL,
				`email` varchar(255) NOT NULL,
				`valid` tinyint(1) NOT NULL DEFAULT '0',
				`notifications` tinyint(1) NOT NULL DEFAULT '1',
				PRIMARY KEY (`id`) );");
tryQuery($createUser);

$createToken = $db->prepare('CREATE TABLE `token` (
					`id` int(11) NOT NULL AUTO_INCREMENT,
					`token` varchar(40) NOT NULL,
					`login` varchar(20) NOT NULL,
					`type` varchar(20) NOT NULL,
					PRIMARY KEY (`id`) );');
tryQuery($createToken);

$createImage = $db->prepare('CREATE TABLE `image` (
					`id` int(11) NOT NULL AUTO_INCREMENT,
					`name` varchar(255) NOT NULL,
					`userID` int(11) NOT NULL,
					`timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
					PRIMARY KEY (`id`),
					FOREIGN KEY (`userID`) REFERENCES user(`id`) );');
tryQuery($createImage);

$createLike = $db->prepare('CREATE TABLE `like` (
					`id` int(11) NOT NULL AUTO_INCREMENT,
					`imageID` int(11) NOT NULL,
					`userID` int(11) NOT NULL,
					PRIMARY KEY (`id`),
					FOREIGN KEY (`userID`) REFERENCES user(`id`),
					FOREIGN KEY (`imageID`) REFERENCES image(`id`) );');
tryQuery($createLike);

$createComment = $db->prepare('CREATE TABLE `comment` (
					`id` int(11) NOT NULL AUTO_INCREMENT,
					`imageID` int(11) NOT NULL,
					`userID` int(11) NOT NULL,
					`text` varchar(255) NOT NULL,
					`timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
					PRIMARY KEY (`id`),
					FOREIGN KEY (`userID`) REFERENCES user(`id`),
					FOREIGN KEY (`imageID`) REFERENCES image(`id`) );');
tryQuery($createComment);