<?php
include_once 'models/base.php';
include_once ROOT_PATH . '/models/notify.php';

if (!isset($_SESSION['login']))
	notify('You cannot signout if you are not even signed in', '/index.php');

include_once ROOT_PATH . '/models/signout.php';

signOut();