<?php
function tryQuery($stmt, $params = null, $location = false) {
	include_once ROOT_PATH . '/models/notify.php';

	try {
		if ($params != null)
			$result = $stmt->execute($params);
		else
			$result = $stmt->execute();
		return $result;
	}
	catch (PDOException $e) {
		notify('Database error : ' . $e->getMessage(), $location);
		return false;
	}
}