<?php
include_once '../config/rootPath.php';
include_once ROOT_PATH . '/models/dbConnect.php';
include_once ROOT_PATH . '/models/tryQuery.php';

if (isset($_POST) && isset($_POST['id'])) {
	session_start();

	$db = dbConnect();
	$id = $_POST['id'];

	if (!is_numeric($id))
		exit;

	$stmt = $db->prepare('SELECT id FROM user WHERE login = :login;');
	$params = array(':login' => $_SESSION['login']);
	tryQuery($stmt, $params);
	$userID = $stmt->fetch()['id'];

	$stmt = $db->prepare('SELECT userID FROM image WHERE id = :id;');
	$params = array(':id' => $id);
	tryQuery($stmt, $params);
	$owner = $stmt->fetch()['userID'];

	if ($userID !== $owner)
		notify('You can not delete photos that you don\'t own.', '/gallery.php');

	$stmt = $db->prepare('DELETE FROM `like` WHERE imageID = :imageID;');
	$params = array(':imageID' => $id);
	tryQuery($stmt, $params);

	$stmt = $db->prepare('DELETE FROM comment WHERE imageID = :imageID;');
	$params = array(':imageID' => $id);
	tryQuery($stmt, $params);

	$stmt = $db->prepare('SELECT name FROM image WHERE id = :id;');
	$params = array(':id' => $id);
	tryQuery($stmt, $params);
	$name = $stmt->fetch()['name'];

	$stmt = $db->prepare('DELETE FROM image WHERE id = :id;');
	$params = array(':id' => $id);
	tryQuery($stmt, $params);

	unlink(ROOT_PATH . '/images/' . $name);
}