<?php
function changeNotifPref($form) {
	include_once ROOT_PATH . '/models/dbConnect.php';
	include_once ROOT_PATH . '/models/tryQuery.php';

	$db = dbConnect();
	$location = '/profile.php';

	$stmt = $db->prepare('SELECT notifications FROM user WHERE login = :login;');
	$params = array(':login' => $_SESSION['login']);
	tryQuery($stmt, $params);
	$pref = $stmt->fetch()['notifications'];

	$stmt = $db->prepare('UPDATE user SET notifications = :notif WHERE login = :login;');
	if (isset($form['notif']) && $form['notif'] === 'yes') {
		if ($pref === '1')
			notify('Preferences updated', $location);
		$params = array(':notif' => 1, ':login' => $_SESSION['login']);
		tryQuery($stmt, $params);
	}
	else {
		if ($pref === '0')
			notify('Preferences updated', $location);
		$params = array(':notif' => 0, ':login' => $_SESSION['login']);
		tryQuery($stmt, $params);
	}
}