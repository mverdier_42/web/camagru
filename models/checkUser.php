<?php
function checkUser($login) {
	include_once ROOT_PATH . '/models/dbConnect.php';
	include_once ROOT_PATH . '/models/tryQuery.php';
	include_once ROOT_PATH . '/models/signout.php';

	if (!($db = dbConnect())) {
		header('Location: /index.php');
		exit ;
	}

	$stmt = $db->prepare('SELECT id FROM user WHERE login = :login;');
	$params = array(':login' => $login);
	tryQuery($stmt, $params, '/index.php');

	if ($stmt->rowCount() < 1)
		signOut();
}