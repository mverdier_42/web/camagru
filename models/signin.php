<?php
function SignIn($form) {
	include_once ROOT_PATH . '/models/dbConnect.php';
	include_once ROOT_PATH . '/models/encryptPassword.php';
	include_once ROOT_PATH . '/models/tryQuery.php';
	include_once ROOT_PATH . '/models/notify.php';

	if (!($db = dbConnect())) {
		header('Location: /connexion.php');
		exit ;
	}
	$location = '/connexion.php';

	$stmt = $db->prepare('SELECT login, password, valid FROM user WHERE login = :login;');
	$params = array(':login' => $form['login']);
	tryQuery($stmt, $params, $location);

	if ($stmt->rowCount() < 1)
		notify('This user does not exist.', $location);

	$row = $stmt->fetch();
	if (encryptPassword($form['password']) !== $row['password'])
		notify('Wrong password.', $location);
	if ($row['valid'] === '0')
		notify('Account not activated yet.', $location);

	$_SESSION['login'] = $form['login'];
	notify('You have been logged in.', '/index.php');
}