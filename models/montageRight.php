<?php
include_once '../config/rootPath.php';
include_once ROOT_PATH . '/models/dbConnect.php';
include_once ROOT_PATH . '/models/tryQuery.php';

$div = '';

if (isset($_POST) && isset($_POST['limit']) && isset($_POST['offset'])) {
	session_start();

	$db = dbConnect();
	$limit = $_POST['limit'];
	$offset = $_POST['offset'];

	if (!is_numeric($limit) || !is_numeric($offset)) {
		echo $div;
		exit ;
	}

	$stmt = $db->prepare('SELECT image.id, image.name FROM image INNER JOIN user
		ON (user.login = :login AND image.userID = user.id)
		ORDER BY image.timestamp DESC LIMIT :limit OFFSET :offset;');
	$stmt->bindValue(':login', $_SESSION['login']);
	$stmt->bindValue(':limit', intval($limit), PDO::PARAM_INT);
	$stmt->bindValue(':offset', intval($offset), PDO::PARAM_INT);
	tryQuery($stmt, null, '/index.php');

	if ($stmt->rowCount() < 1) {
		echo 'end';
		exit ;
	}

	$images = $stmt->fetchAll();

	foreach ($images as $image) {
		$src = $image['name'];
		if (!file_exists(ROOT_PATH . '/images/' . $src))
			$src = 'notFound.png';
		$div .= '<div class=\'miniature\'>';
		$div .= '<img class=\'sideImages\' src=\'/images/' . $src . '\' >';
		$div .= '<div class=\'delete\' onclick=\'deleteMiniature(' . $image['id'] . ')\'>X</div>';
		$div .= '</div>';
	}
}

echo $div;