<?php
function sendResetPassword($email) {
	include_once ROOT_PATH . '/models/dbConnect.php';
	include_once ROOT_PATH . '/models/tryQuery.php';
	include_once ROOT_PATH . '/models/notify.php';

	if (!($db = dbConnect())) {
		header('Location: /connexion.php');
		exit ;
	}
	$location = '/connexion.php';

	if (empty($email))
		notify('Please enter a valid email.', $location);

	$stmt = $db->prepare('SELECT login FROM user WHERE email = :email;');
	$params = array(':email' => $email);
	tryQuery($stmt, $params, $location);

	if ($stmt->rowCount() < 1)
		notify('No account registered within this email.', $location);

	$login = $stmt->fetch()['login'];

	$token = sha1(uniqid($login, true));
	$rootUrl = (!empty($_SERVER['HTTPS']) ? 'https' : 'http');
	$rootUrl .= '://' . $_SERVER['HTTP_HOST'];

	$message = "
	<html>
		<body>
			<a href='$rootUrl/reset_password.php?token=$token' target='_blank'>Reset your password !</a>
		</body>
	</html>";
	$headers[] = 'MIME-Version: 1.0';
	$headers[] = 'Content-type: text/html; charset=iso-8859-1';
	$headers[] = 'From: mverdier@student.42.fr';
	$headers[] = 'To: ' . $email;

	mail($email, 'Reset your password', $message, implode("\r\n", $headers));

	$type = 'reset';
	$stmt = $db->prepare('SELECT login FROM token WHERE login = :login AND type = :type;');
	$params = array(':login' => $login, ':type' => $type);
	tryQuery($stmt, $params);

	if ($stmt->rowCount() > 0) {
		$stmt = $db->prepare('UPDATE token SET token = :token WHERE login = :login AND type = :type');
		$params = array(':token' => $token, ':login' => $login, ':type' => $type);
		tryQuery($stmt, $params, $location);
	}
	else {
		$stmt = $db->prepare('INSERT INTO token (token, login, type) VALUES (:token, :login, :type);');
		$params = array(':token' => $token, ':login' => $login, ':type' => $type);
		tryQuery($stmt, $params, $location);
	}

	notify('A reset link has been sent to your email.', $location);
}