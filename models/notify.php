<?php
function notify($message, $redirection = false) {
	$_SESSION['notifications'][] = $message;

	if ($redirection !== false) {
		header('Location: ' . $redirection);
		exit;
	}
}