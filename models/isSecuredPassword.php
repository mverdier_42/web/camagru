<?php
function isSecuredPassword(string $password) {
	$minLength = 8;
	$minUpcases = 1;
	$minDigits = 1;

	$upcases = 0;
	$digits = 0;

	$chars = str_split($password);

	foreach ($chars as $char) {
		if (ctype_digit($char))
			$digits++;
		if (ctype_upper($char))
			$upcases++;
	}

	if (strlen($password) < $minLength ||
		$digits < $minDigits || $upcases < $minUpcases)
		return false;

	return true;
}