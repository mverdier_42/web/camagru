<?php
function dbConnect($dsn = false) {
	include ROOT_PATH . '/config/database.php';
	include_once ROOT_PATH . '/models/notify.php';

	try {
		if ($dsn !== false)
			$db = new PDO($dsn, $DB_USER, $DB_PASSWORD);
		else
			$db = new PDO($DB_DSN, $DB_USER, $DB_PASSWORD);
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}
	catch (PDOException $e) {
		notify('Can\'t connect to database : ' . $e->getMessage());
		return false;
	}
	return $db;
}