<?php
function resetPassword($form) {
	include_once ROOT_PATH . '/models/dbConnect.php';
	include_once ROOT_PATH . '/models/tryQuery.php';
	include_once ROOT_PATH . '/models/notify.php';
	include_once ROOT_PATH . '/models/isSecuredPassword.php';
	include_once ROOT_PATH . '/models/encryptPassword.php';

	if (!isset($form['token']))
		notify('Invalid token.', '/index.php');
	
	$login = checkResetPasswordToken($form['token'], true);

	if (!isSecuredPassword($form['password']) ||
		$form['password'] === $login) {
		notify('New password is not secured.', '/reset_password.php?token=' . $form['token']);
	}

	if (!($db = dbConnect())) {
		header('Location: /index.php');
		exit ;
	}
	$location = '/connexion.php';

	$stmt = $db->prepare('UPDATE user SET password = :password WHERE login = :login;');
	$params = array(':password' => encryptPassword($form['password']),
		':login' => $login);
	tryQuery($stmt, $params, $location);

	$stmt = $db->prepare('DELETE FROM `token` WHERE `token`.`token` = :token;');
	$params = array(':token' => $form['token']);
	tryQuery($stmt, $params, $location);

	notify('Your password has successfully been changed.', $location);
}