<?php
include_once '../config/rootPath.php';
include_once ROOT_PATH . '/models/dbConnect.php';
include_once ROOT_PATH . '/models/tryQuery.php';

if (isset($_POST) && isset($_POST['image']) && isset($_POST['filter'])
	&& isset($_POST['width']) && isset($_POST['height'])) {
	if (!isset($_SESSION))
		session_start();
	
	if (!is_numeric($_POST['width']) || !is_numeric($_POST['height']))
		return false;

	$db = dbConnect();
	$location = '/montage.php';
	$path = ROOT_PATH . '/images/';

	if (!file_exists($path))
		mkdir($path);

	$date = new DateTime();
	$name = sha1(uniqid($date->getTimestamp())) . '.jpeg';
	$path .= $name;

	$imageURI = explode(',', $_POST['image']);
	$imageURI = str_replace(' ', '+', $imageURI[1]);
	$image = base64_decode($imageURI);

	$filterURI = explode(',', $_POST['filter']);
	$filterURI = str_replace(' ', '+', $filterURI[1]);
	$filter = base64_decode($filterURI);

	$image = imagecreatefromstring($image);
	$filter = imagecreatefromstring($filter);

	imagealphablending($image, true);
	imagealphablending($filter, true);
	imagesavealpha($image, true);
	imagesavealpha($filter, true);

	imagecopy($image, $filter, 0, 0, 0, 0, $_POST['width'], $_POST['height']);
	imagepng($image, $path);

	imagedestroy($image);
	imagedestroy($filter);

	$stmt = $db->prepare('SELECT id FROM user WHERE login = :login;');
	$params = array(':login' => $_SESSION['login']);
	tryQuery($stmt, $params, $location);
	$userID = $stmt->fetch()['id'];

	$stmt = $db->prepare('INSERT INTO image (name, userID) VALUES (:name, :userID);');
	$params = array(':name' => $name, ':userID' => $userID);
	tryQuery($stmt, $params, $location);

	return true;
}