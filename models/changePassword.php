<?php
function changePassword($form) {
	include_once ROOT_PATH . '/models/dbConnect.php';
	include_once ROOT_PATH . '/models/tryQuery.php';
	include_once ROOT_PATH . '/models/encryptPassword.php';
	include_once ROOT_PATH . '/models/isSecuredPassword.php';
	include_once ROOT_PATH . '/models/notify.php';

	$db = dbConnect();
	$location = '/profile.php';

	$oldPassword = encryptPassword($form['oldPassword']);
	$newPassword = encryptPassword($form['newPassword']);
	$login = $_SESSION['login'];

	$stmt = $db->prepare('SELECT password FROM user WHERE login = :login;');
	$params = array(':login' => $login);
	tryQuery($stmt, $params, $location);

	$row = $stmt->fetch();
	if ($oldPassword !== $row['password'])
		notify('Old password is not correct.', $location);
	else if ($newPassword === $row['password'])
		notify('New password can\'t be old password.', $location);

	if (!isSecuredPassword($form['newPassword']) ||
		$form['newPassword'] === $login)
		notify('Password unsecured.', $location);

	$stmt = $db->prepare('UPDATE user SET password = :newPassword WHERE login = :login;');
	$params = array(':newPassword' => $newPassword, ':login' => $login);
	tryQuery($stmt, $params, $location);

	notify('Password changed.', $location);
}