<?php
include_once '../config/rootPath.php';
include_once ROOT_PATH . '/models/dbConnect.php';
include_once ROOT_PATH . '/models/tryQuery.php';
include_once ROOT_PATH . '/models/notify.php';
include_once ROOT_PATH . '/models/getCommentsNb.php';

session_start();
if (!isset($_SESSION['login'])) {
	notify('You need to be logged to like.');
	echo 'false';
	exit;
}

if (isset($_POST) && isset($_POST['id'])) {
	if (!is_numeric($_POST['id'])) {
		echo '';
		exit ;
	}

	$db = dbConnect();
	$imageID = $_POST['id'];

	$stmt = $db->prepare('SELECT id FROM user WHERE login = :login;');
	$params = array(':login' => $_SESSION['login']);
	tryQuery($stmt, $params);
	$userID = $stmt->fetch()['id'];

	$stmt = $db->prepare('SELECT userID FROM image WHERE id = :id;');
	$params = array(':id' => $imageID);
	tryQuery($stmt, $params);
	$owner = $stmt->fetch()['userID'];

	$stmt = $db->prepare('SELECT `like`.id FROM `like`
		INNER JOIN user ON `like`.userID = user.id AND user.id = :userID
		INNER JOIN image ON `like`.imageID = image.id AND image.id = :imageID;');
	$params = array(':userID' => $userID, ':imageID' => $imageID);
	tryQuery($stmt, $params);

	$rootUrl = (!empty($_SERVER['HTTPS']) ? 'https' : 'http');
	$rootUrl .= '://' . $_SERVER['HTTP_HOST'];

	if ($stmt->rowCount() < 1) {
		$stmt = $db->prepare('INSERT INTO `like` (userID, imageID) VALUES (:userID, :imageID);');
		$params = array(':userID' => $userID, ':imageID' => $imageID);
		tryQuery($stmt, $params);
		if ($userID != $owner) {
			$stmt = $db->prepare('SELECT email, notifications FROM user WHERE id = :id;');
			$params = array(':id' => $owner);
			tryQuery($stmt, $params);
			$result = $stmt->fetch();

			$email = $result['email'];
			$notifications = $result['notifications'];
			if ($notifications === '1') {
				$message = "
				<html>
					<body>
						Hey ! An other user from camagru liked <a href='$rootUrl/gallery.php?id=$imageID' target='_blank'>one of your photos</a>
					</body>
				</html>";
				$headers[] = 'MIME-Version: 1.0';
				$headers[] = 'Content-type: text/html; charset=iso-8859-1';
				$headers[] = 'From: mverdier@student.42.fr';
				$headers[] = 'To: ' . $email;

				mail($email, 'You got a new like', $message, implode("\r\n", $headers));
			}
		}
		echo 'like';
	}
	else {
		$stmt = $db->prepare('DELETE FROM `like` WHERE userID = :userID AND imageID = :imageID;');
		$params = array(':userID' => $userID, ':imageID' => $imageID);
		tryQuery($stmt, $params);
		echo 'unlike';
	}
}