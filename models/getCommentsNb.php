<?php
function getCommentsNb($imageID) {
	include_once ROOT_PATH . '/models/dbConnect.php';
	include_once ROOT_PATH . '/models/tryQuery.php';

	$db = dbConnect();

	$stmt = $db->prepare('SELECT COUNT(comment.id) AS nb FROM comment INNER JOIN `image`
		ON comment.imageID = `image`.id WHERE `image`.id = :imageID;');
	$params = array(':imageID' => $imageID);
	tryQuery($stmt, $params, '/index.php');
	$comments = $stmt->fetch()['nb'];

	return $comments;
}