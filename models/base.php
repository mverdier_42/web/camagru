<?php
if (!isset($_SESSION))
	session_start();

include_once 'config/rootPath.php';
include_once ROOT_PATH . '/models/checkUser.php';

if (isset($_SESSION['login']))
	checkUser($_SESSION['login']);

include_once ROOT_PATH . '/views/header.php';
include_once ROOT_PATH . '/views/footer.php';
include_once ROOT_PATH . '/views/notifications.php';

$_SESSION['notifications'] = array();