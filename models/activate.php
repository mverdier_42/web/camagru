<?php
function activate($token) {
	include_once ROOT_PATH . '/models/dbConnect.php';
	include_once ROOT_PATH . '/models/tryQuery.php';

	if (!($db = dbConnect())) {
		header('Location: /index.php');
		exit ;
	}

	$stmt = $db->prepare('SELECT login, type FROM token WHERE token = :token;');
	$params = array(':token' => $token);
	tryQuery($stmt, $params, '/index.php');

	if ($stmt->rowCount() < 1)
		notify('Your link is not valid', '/index.php');

	$row = $stmt->fetch();
	if ($row['type'] !== 'activate')
		notify('Your link is not valid', '/index.php');
	$login = $row['login'];

	$stmt = $db->prepare('DELETE FROM `token` WHERE `token`.`token` = :token;');
	$params = array(':token' => $token);
	tryQuery($stmt, $params, '/index.php');

	$stmt = $db->prepare('UPDATE user SET valid = :valid WHERE login = :login;');
	$params = array(':valid' => 1, ':login' => $login);
	tryQuery($stmt, $params, '/index.php');

	notify('Your account has successfully been activated', '/connexion.php');
}