<?php
function checkResetPasswordToken($token, $retLogin = false) {
	include_once ROOT_PATH . '/models/dbConnect.php';
	include_once ROOT_PATH . '/models/tryQuery.php';
	include_once ROOT_PATH . '/models/notify.php';

	if (!($db = dbConnect())) {
		header('Location: /index.php');
		exit ;
	}

	$stmt = $db->prepare('SELECT login, type FROM token WHERE token = :token;');
	$params = array(':token' => $token);
	tryQuery($stmt, $params, '/index.php');

	if ($stmt->rowCount() < 1)
		notify('Invalid token', '/index.php');

	$row = $stmt->fetch();
	if ($row['type'] !== 'reset')
		notify('Invalid token', '/index.php');
	if ($retLogin === true)
		return $row['login'];

	return $token;
}