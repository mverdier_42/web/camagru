<?php
function getLikesNb($imageID) {
	include_once ROOT_PATH . '/models/dbConnect.php';
	include_once ROOT_PATH . '/models/tryQuery.php';

	$db = dbConnect();

	$stmt = $db->prepare('SELECT COUNT(`like`.id) AS nb FROM `like` INNER JOIN `image`
		ON `like`.imageID = `image`.id AND `image`.id = :imageID;');
	$params = array(':imageID' => $imageID);
	tryQuery($stmt, $params, '/index.php');
	$likes = $stmt->fetch()['nb'];

	return $likes;
}