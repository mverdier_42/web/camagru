<?php
function changeEmail($form) {
	include_once ROOT_PATH . '/models/dbConnect.php';
	include_once ROOT_PATH . '/models/tryQuery.php';

	$db = dbConnect();
	$location = '/profile.php';

	$stmt = $db->prepare('SELECT login FROM user WHERE email = :email;');
	$params = array(':email' => $form['email']);
	tryQuery($stmt, $params);

	if ($stmt->rowCount() > 0)
		notify('This email is already used.', $location);
	
	$stmt = $db->prepare('UPDATE user SET email = :email WHERE login = :login;');
	$params = array(':email' => $form['email'], ':login' => $_SESSION['login']);
	tryQuery($stmt, $params);

	notify('Email successfully changed.', $location);
}