<?php
include_once '../config/rootPath.php';
include_once ROOT_PATH . '/models/getLikesNb.php';
include_once ROOT_PATH . '/models/getCommentsNb.php';
include_once ROOT_PATH . '/models/dbConnect.php';
include_once ROOT_PATH . '/models/tryQuery.php';

if (isset($_POST) && isset($_POST['limit']) && isset($_POST['offset'])) {
	session_start();

	$db = dbConnect();
	$limit = $_POST['limit'];
	$offset = $_POST['offset'];
	$login = (isset($_SESSION['login']) ? $_SESSION['login'] : '');

	if (!is_numeric($limit) || !is_numeric($offset)) {
		echo '';
		exit ;
	}

	$stmt = $db->prepare('SELECT id FROM user WHERE login = :login;');
	$params = array(':login' => $login);
	tryQuery($stmt, $params);
	$userID = $stmt->fetch()['id'];

	$stmt = $db->prepare('SELECT id, name FROM image ORDER BY timestamp DESC LIMIT :limit OFFSET :offset');
	$stmt->bindValue(':limit', intval($limit), PDO::PARAM_INT);
	$stmt->bindValue(':offset', intval($offset), PDO::PARAM_INT);
	tryQuery($stmt, null, '/index.php');

	if ($stmt->rowCount() < 1) {
		echo 'end';
		exit ;
	}

	$images = $stmt->fetchAll();

	$html = '';

	$i = 0;
	foreach ($images as $image) {
		$id = $image['id'];
		$src = $image['name'];
		$likesNb = getLikesNb($id);
		$commentsNb = getCommentsNb($id);
	
		if (!file_exists(ROOT_PATH . '/images/' . $src))
			$src = 'notFound.png';

		$stmt = $db->prepare('SELECT userID, `timestamp` FROM image WHERE id = :id;');
		$params = array(':id' => $image['id']);
		tryQuery($stmt, $params);
		$result = $stmt->fetch();
		$owner = $result['userID'];
		$timestamp = $result['timestamp'];

		$stmt = $db->prepare('SELECT login FROM user WHERE id = :userID');
		$params = array(':userID' => $owner);
		tryQuery($stmt, $params);
		$ownerLogin = $stmt->fetch()['login'];
		if (strlen($ownerLogin) > 17)
			$ownerShortLogin = substr($ownerLogin, 0, 15) . '..';
		else
			$ownerShortLogin = $ownerLogin;

		$owner = ($userID === $owner ? 'true' : 'false');
		$html .= "<div class='galleryImg, imgs' onclick='displayImg($id,
				\"$src\", $owner, $likesNb, $commentsNb, \"$timestamp\", \"$ownerLogin\")'>
			<img class='galleryImgSrc' src='/images/$src' >
			<br />
			<div class='imageInfos'>[$timestamp] $ownerShortLogin</div>
			<div class='galleryImg'>&#128077 $likesNb</div>
			<div class='galleryImg'>&#128172 $commentsNb</div>
		</div>";
		$i++;
	}

	echo $html;
}