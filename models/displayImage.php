<?php
function displayImage($id) {
	include_once ROOT_PATH . '/models/getLikesNb.php';
	include_once ROOT_PATH . '/models/getCommentsNb.php';
	include_once ROOT_PATH . '/models/dbConnect.php';
	include_once ROOT_PATH . '/models/tryQuery.php';

	$db = dbConnect();
	$likesNb = getLikesNb($id);
	$commentsNb = getCommentsNb($id);

	$stmt = $db->prepare('SELECT id FROM user WHERE login = :login;');
	$params = array(':login' => $_SESSION['login']);
	tryQuery($stmt, $params);
	$userID = $stmt->fetch()['id'];

	$stmt = $db->prepare('SELECT name, userID, `timestamp` FROM image WHERE id = :id;');
	$params = array(':id' => $id);
	tryQuery($stmt, $params);

	if ($stmt->rowCount() < 1)
		return "onload='startup()'";

	$result = $stmt->fetch();
	$src = $result['name'];
	$owner = $result['userID'];
	$timestamp = $result['timestamp'];

	$stmt = $db->prepare('SELECT login FROM user WHERE id = :userID');
	$params = array(':userID' => $owner);
	tryQuery($stmt, $params);
	$ownerLogin = $stmt->fetch()['login'];

	$owner = ($userID === $owner ? 'true' : 'false');
	$html = "onload='startup($id, \"$src\", $owner, $likesNb, $commentsNb, \"$timestamp\", \"$ownerLogin\", displayImg)'";

	return $html;
}