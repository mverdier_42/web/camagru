<?php
include_once '../config/rootPath.php';
include_once ROOT_PATH . '/models/dbConnect.php';
include_once ROOT_PATH . '/models/tryQuery.php';

$html = '';

if (isset($_POST) && isset($_POST['id'])) {
	if (!is_numeric($_POST['id'])) {
		echo $html;
		exit ;
	}
	$db = dbConnect();

	$stmt = $db->prepare('SELECT comment.userID, comment.text, comment.timestamp
		FROM comment INNER JOIN image
		ON comment.imageID = image.id AND image.id = :imageID
		ORDER BY comment.timestamp ASC;');
	$params = array(':imageID' => $_POST['id']);
	tryQuery($stmt, $params);
	$result = $stmt->fetchAll();

	foreach ($result as $row) {
		$stmt = $db->prepare('SELECT login FROM user WHERE id = :id;');
		$params = array(':id' => $row['userID']);
		tryQuery($stmt, $params);
		$login = $stmt->fetch()['login'];
		$html .= '<div class=\'leftText\'>';
		$html .= '[' . $row['timestamp'] . '] ' . $login . ':<br />' . htmlspecialchars($row['text']) . '<hr></div>';
	}
}

echo $html;