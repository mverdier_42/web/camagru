<?php
function SignUp($form) {
	include_once ROOT_PATH . '/models/dbConnect.php';
	include_once ROOT_PATH . '/models/encryptPassword.php';
	include_once ROOT_PATH . '/models/tryQuery.php';
	include_once ROOT_PATH . '/models/isSecuredPassword.php';
	include_once ROOT_PATH . '/models/notify.php';

	$location = '/connexion.php';
	$login = $form['login'];
	$email = $form['email'];

	if (strlen($login) > 20)
		notify('Login should be 25 characters max.', $location);
	if (strlen($login) < 3)
		notify('Login should be 3 characters min.', $location);
	if (empty($email))
		notify('Please enter a valid email.', $location);

	if (!($db = dbConnect())) {
		header('Location: ' . $location);
		exit ;
	}

	$stmt = $db->prepare('SELECT login, email FROM user WHERE login = :login OR email = :email;');
	$params = array(':login' => $login, ':email' => $email);
	tryQuery($stmt, $params, $location);
	$result = $stmt->fetchAll();

	foreach ($result as $row) {
		if ($form['login'] === $row['login'])
			notify('This user already exists.', $location);
		else if ($form['email'] === $row['email'])
			notify('This email is already in use.', $location);
	}

	if (!isSecuredPassword($form['password']) ||
		$form['password'] === $form['login'])
		notify('Password unsecured.', $location);

	$stmt = $db->prepare('INSERT INTO user (login, password, email)
		VALUES (:login, :password, :email);');
	$params = array(':login' => $login,
		':password' => encryptPassword($form['password']), ':email' => $email);
	tryQuery($stmt, $params, $location);

	$token = sha1(uniqid($login, true));
	$rootUrl = (!empty($_SERVER['HTTPS']) ? 'https' : 'http');
	$rootUrl .= '://' . $_SERVER['HTTP_HOST'];

	$message = "
	<html>
		<body>
			<a href='$rootUrl/activate.php?token=$token' target='_blank'>Activate your account !</a>
		</body>
	</html>";
	$headers[] = 'MIME-Version: 1.0';
	$headers[] = 'Content-type: text/html; charset=iso-8859-1';
	$headers[] = 'From: mverdier@student.42.fr';
	$headers[] = 'To: ' . $email;

	mail($email, 'Activate your account', $message, implode("\r\n", $headers));

	$stmt = $db->prepare('INSERT INTO token (token, login, type) VALUES (:token, :login, :type);');
	$params = array(':token' => $token, ':login' => $login, ':type' => 'activate');
	tryQuery($stmt, $params, $location);

	notify('Check your mails to valid your account !', $location);
}