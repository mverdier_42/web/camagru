<?php
function signOut() {
	if (isset($_SESSION['login']))
		session_destroy();
	header('Location: /index.php');
	exit;
}