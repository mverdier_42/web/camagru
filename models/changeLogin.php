<?php
function changeLogin($form) {
	include_once ROOT_PATH . '/models/dbConnect.php';
	include_once ROOT_PATH . '/models/tryQuery.php';
	include_once ROOT_PATH . '/models/notify.php';

	$db = dbConnect();
	$location = '/profile.php';

	$oldLogin = $_SESSION['login'];
	$newLogin = $form['login'];

	$stmt = $db->prepare('SELECT login FROM user WHERE login = :oldLogin;');
	$params = array(':oldLogin' => $oldLogin);
	tryQuery($stmt, $params, $location);

	if ($stmt->rowCount() > 0)
		notify('Bad login.', $location);

	$stmt = $db->prepare('UPDATE user SET login = :newLogin WHERE login = :oldLogin;');
	$params = array(':newLogin' => $newLogin, ':oldLogin', $oldLogin);
	tryQuery($stmt, $params, $location);

	notify('Login successfully changed.', $location);
}