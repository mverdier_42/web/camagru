<?php
include_once '../config/rootPath.php';
include_once ROOT_PATH . '/models/dbConnect.php';
include_once ROOT_PATH . '/models/tryQuery.php';
include_once ROOT_PATH . '/models/notify.php';
include_once ROOT_PATH . '/models/getCommentsNb.php';

session_start();
if (!isset($_SESSION['login'])) {
	notify('You need to be logged to comment.');
	echo 'false';
	exit;
}

$html = '';

if (isset($_POST) && isset($_POST['id']) && isset($_POST['text'])) {
	if (!is_numeric($_POST['id'])) {
		echo $html;
		exit ;
	}

	$db = dbConnect();
	$imageID = $_POST['id'];
	$text = $_POST['text'];
	$sender = $_SESSION['login'];

	$stmt = $db->prepare('SELECT id FROM user WHERE login = :login;');
	$params = array(':login' => $sender);
	tryQuery($stmt, $params);
	$userID = $stmt->fetch()['id'];

	$stmt = $db->prepare('SELECT userID FROM image WHERE id = :id;');
	$params = array(':id' => $imageID);
	tryQuery($stmt, $params);
	$owner = $stmt->fetch()['userID'];

	$stmt = $db->prepare('INSERT INTO comment (imageID, userID, text)
		VALUES (:id, :userID, :comment);');
	$params = array(':id' => $imageID, ':userID' => $userID, ':comment' => $text);
	if (!tryQuery($stmt, $params)) {
		echo 'false';
		exit ;
	}

	$text = htmlspecialchars($text);

	$stmt = $db->prepare('SELECT CURRENT_TIMESTAMP();');
	tryQuery($stmt);
	$time = $stmt->fetch()[0];

	$html .= '<div class=\'leftText\'>';
	$html .= '[' . $time . '] ' . $sender . ':<br />' . $text . '<hr></div>';

	$rootUrl = (!empty($_SERVER['HTTPS']) ? 'https' : 'http');
	$rootUrl .= '://' . $_SERVER['HTTP_HOST'];

	if ($userID != $owner) {
		$stmt = $db->prepare('SELECT email, notifications FROM user WHERE id = :id;');
		$params = array(':id' => $owner);
		tryQuery($stmt, $params);
		$result = $stmt->fetch();

		$email = $result['email'];
		$notifications = $result['notifications'];
		if ($notifications === '1') {
			$message = "
			<html>
				<body>
					Hey ! An other user from camagru commented <a href='$rootUrl/gallery.php?id=$imageID' target='_blank'>one of your photos</a> :
					<br />
					(From: $sender)
					<br />
					$text
				</body>
			</html>";
			$headers[] = 'MIME-Version: 1.0';
			$headers[] = 'Content-type: text/html; charset=iso-8859-1';
			$headers[] = 'From: mverdier@student.42.fr';
			$headers[] = 'To: ' . $email;

			mail($email, 'You got a new comment', $message, implode("\r\n", $headers));
		}
	}
}

echo $html;