<?php
include_once '../config/rootPath.php';
include_once ROOT_PATH . '/models/dbConnect.php';
include_once ROOT_PATH . '/models/tryQuery.php';

session_start();

$db = dbConnect();

$stmt = $db->prepare('SELECT notifications FROM user WHERE login = :login;');
$params = array(':login' => $_SESSION['login']);
tryQuery($stmt, $params);
$pref = $stmt->fetch()['notifications'];

if ($pref === '1')
	echo 'true';
else
	echo 'false';