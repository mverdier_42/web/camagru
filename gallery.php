<?php
include_once 'models/base.php';
include_once ROOT_PATH . '/models/displayImage.php';
include_once ROOT_PATH . '/models/dbConnect.php';
include_once ROOT_PATH . '/models/tryQuery.php';

if (!($db = dbConnect())) {
	header('Location: /index.php');
	exit ;
}

$stmt = $db->prepare('SELECT * FROM `user` LIMIT 1');
tryQuery($stmt, null, '/index.php');

$stmt = $db->prepare('SELECT * FROM `image` LIMIT 1');
tryQuery($stmt, null, '/index.php');

$stmt = $db->prepare('SELECT * FROM `like` LIMIT 1');
tryQuery($stmt, null, '/index.php');

$stmt = $db->prepare('SELECT * FROM `comment` LIMIT 1');
tryQuery($stmt, null, '/index.php');

$onload = '';

if (isset($_GET) && isset($_GET['id']) && is_numeric($_GET['id']))
	$onload = displayImage($_GET['id']);
else
	$onload = "onload='startup()'";

include_once ROOT_PATH . '/views/gallery.php';