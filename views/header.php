<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="/style.css" />
		<link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16" />
		<link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32" />
	</head>

	<body>
		<div class='header'>
			<a href='/index.php'>Home</a>
			<a href='/gallery.php'>Gallery</a>
			<a href='/montage.php'>Montage</a>
			<?php if (!isset($_SESSION['login'])) { ?>
				<a href='/connexion.php'>Connexion</a>
			<?php } else { ?>
				<a href='/profile.php'>Profile</a>
				<a href='/signout.php'>Sign out</a>
			<?php } ?>
		</div>
	</body>
</html>