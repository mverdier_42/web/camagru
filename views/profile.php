<!DOCTYPE html>
<html>
	<head>
		<title>Profile</title>
		<link rel="stylesheet" type="text/css" href="/style.css" />
		<script src='/scripts/profile.js'></script>
	<head>

	<body>
		<div class='margintop'>
			<div class='profile'>
				<p><u>Change login :</u></p>
				<form action='/profile.php' method='post'>
					<label for='login'>New login :</label>
					<input type='text' name='login' placeholder='Enter login' />
					<br />
					<input type='submit' name='submit' value='Change login' />
				</form>
			</div>
			<div class='profile'>
				<p><u>Change email :</u></p>
				<form action='/profile.php' method='post'>
					<label for='email'>New email :</label>
					<input type='email' name='email' placeholder='Enter email' />
					<br />
					<input type='submit' name='submit' value='Change email' />
				</form>
			</div>
			<div class='profile'>
				<p><u>Change password :</u></p>
				<form action='/profile.php' method='post'>
					<label for='password'>Old password :</label>
					<input type='password' name='oldPassword' placeholder='Enter Password' />
					<br />
					<label for='password'>New password :</label>
					<input type='password' name='newPassword' placeholder='Enter Password' />
					<br />
					<input type='submit' name='submit' value='Change password' />
				</form>
			</div>
			<div class='profile'>
				<form action='/profile.php' method='post'>
					<label for='notif'>Allow email notification :</label>
					<input type='checkbox' id='notif' name='notif' value='yes'>
					<br />
					<input type='submit' name='submit' value='Update'>
				</form>
			</div>
		</div>
	</body>
</html>