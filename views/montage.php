<!DOCTYPE html>
<html>
	<head>
		<title>Montage</title>
		<link rel="stylesheet" type="text/css" href="/style.css" />
		<script src='/scripts/webcam.js'></script>
	<head>

	<body>
		<div id='montage'>
			<div id='montageLeft'>
				<div id='montageLeftMargin'>
					<label for='image'>Upload image :</label>
					<input id='uploadedImage' type='file' accept='image/*' name='image' />
					<button id='upload'>Upload</button>
					<div id='camera'>
						<video id='video'>Video stream not available</video>
					</div>
					<div id='filters'>
						<div id='filters_container'>
							<img class='filters' src='/filters/filter1.png' width='100px'>
							<img class='filters' src='/filters/filter2.png' width='100px'>
							<img class='filters' src='/filters/filter3.png' width='100px'>
							<img class='filters' src='/filters/filter4.png' width='100px'>
							<img class='filters' src='/filters/filter5.png' width='100px'>
							<img class='filters' src='/filters/filter6.png' width='100px'>
							<img class='filters' src='/filters/filter7.png' width='100px'>
							<img class='filters' src='/filters/filter8.png' width='100px'>
							<img class='filters' src='/filters/filter9.png' width='100px'>
							<img class='filters' src='/filters/filter10.png' width='100px'>
							<img class='filters' src='/filters/filter11.png' width='100px'>
						</div>
					</div>
					<button id='capture' disabled>Take photo</button>
					<canvas id='canvas1'></canvas>
					<canvas id='canvas2'></canvas>
					<div id='output'>
						<img id='photoFilter'>
						<img id='photo' alt='The photo will appear here.'>
					</div>
					<button id='save' disabled>Save</button>
				</div>
			</div>
			<div id='montageRight'>
				<div id='montageRightDiv'>
					<div id='montageRightDivContent'>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>