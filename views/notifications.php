<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="/style.css" />
		<script src='/scripts/notifications.js'></script>
	<head>

	<body>
		<?php if(isset($_SESSION['notifications'])) { 
			foreach ($_SESSION['notifications'] as $notification) { ?>
			<p class='notifications'><?php echo htmlspecialchars($notification); ?></p>
		<?php }} ?>
	</body>
</html>