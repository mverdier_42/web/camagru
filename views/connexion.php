<!DOCTYPE html>
<html>
	<head>
		<title>Connexion</title>
		<link rel="stylesheet" type="text/css" href="/style.css" />
	<head>

	<body>
		<div class='margintop'>
			<div class='profile'>
				<p><u>Sign in :</u></p>
				<form action='/connexion.php' method='post'>
					<label for='login'>Login :</label>
					<input type='text' name='login' placeholder='Enter login' />
					<br />
					<label for='password'>Password :</label>
					<input type='password' name='password' placeholder='Enter Password' />
					<br />
					<input type='submit' name='submit' value='Sign in' />
				</form>
			</div>
			<div class='profile'>
				<p><u>Or sign up :</u></p>
				<form action='/connexion.php' method='post'>
					<label for='login'>Login :</label>
					<input type='text' name='login' placeholder='Enter login' />
					<br />
					<label for='emaim'>Email :</label>
					<input type='email' name='email' placeholder='Enter Email' />
					<br />
					<label for='password'>Password :</label>
					<input type='password' name='password' placeholder='Enter Password' />
					<br />
					<input type='submit' name='submit' value='Sign up' />
				</form>
			</div>
			<div class='profile'>
				<p><u>Forgot password :</u></p>
				<form action='/connexion.php' method='post'>
					<label for='emaim'>Email :</label>
					<input type='email' name='email' placeholder='Enter Email' />
					<br />
					<input type='submit' name='submit' value='Reset password' />
				</form>
			</div>
		</div>
	</body>
</html