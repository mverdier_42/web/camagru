<!DOCTYPE html>
<html>
	<head>
		<title>Gallery</title>
		<link rel="stylesheet" type="text/css" href="/style.css" />
		<script src='/scripts/gallery.js'></script>
	<head>

	<body <?php echo $onload; ?> >
		<div id='galleryContainer' class='galleryOuter'>
			<div id='gallery' class='galleryInner'>
			</div>
		</div>
	</body>
</html>