<!DOCTYPE html>
<html>
	<head>
		<title>Reset password</title>
		<link rel="stylesheet" type="text/css" href="/style.css" />
	<head>

	<body>
		<div class='margintop'>
			<div class='profile'>
				<form action='/reset_password.php' method='post'>
					<label for='password'>New password :</label>
					<input type='password' name='password' placeholder='Enter Password' />
					<br />
					<input type='hidden' name='token' value='<?php echo htmlspecialchars($token); ?>' />
					<input type='submit' name='submit' value='Submit' />
				</form>
			</div>
		</div>
	</body>
</html>