<?php
include_once 'models/base.php';

if (!isset($_SESSION['login'])) {
	header('Location: index.php');
	exit;
}

include_once ROOT_PATH . '/models/changeLogin.php';
include_once ROOT_PATH . '/models/changePassword.php';
include_once ROOT_PATH . '/models/changeNotifPref.php';
include_once ROOT_PATH . '/models/changeEmail.php';

if (isset($_POST) && isset($_POST['submit']) && $_POST['submit'] === 'Change login')
	changeLogin($_POST);
if (isset($_POST) && isset($_POST['submit']) && $_POST['submit'] === 'Change email')
	changeEmail($_POST);
if (isset($_POST) && isset($_POST['submit']) && $_POST['submit'] === 'Change password')
	changePassword($_POST);
if (isset($_POST) && isset($_POST['submit']) && $_POST['submit'] === 'Update')
	changeNotifPref($_POST);

include_once ROOT_PATH . '/views/profile.php';