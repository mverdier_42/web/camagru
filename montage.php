<?php
include_once 'models/base.php';
include_once ROOT_PATH . '/models/notify.php';

if (!isset($_SESSION['login'])) {
	notify('You need to be connected to access montage', '/index.php');
}

include_once ROOT_PATH . '/views/montage.php';